<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	//C
	'conf' => 'Configuration',
	'conf_public' => 'Affichage public',

	//E
	'erreur_groupes_mots' => 'Vous devez avoir cr&eacute;&eacute; au moins un groupe de mots-cl&eacute;s pour configurer la Recherche par mots-cl&eacute;s',

	//G
	'groupes_mots' => 'Choix des groupes de mots-cl&eacute;s',

	//N
	'nombre_colonnes' => 'Choix du nombre de colonnes :',

	//S
	'signature' => '<strong>Recherche multicrit&egrave;res par mots-cl&eacute;s v.0.4</strong><br />
			Ce plugin vous permet d&rsquo;effectuer une recherche d&rsquo;articles en s&eacute;lectionnant plusieurs
			groupes de mots-cl&eacute;s.<br /><br />
			Il faut s&eacute;lectionner les groupes de mots cl&eacute;s ainsi que le type d&rsquo;affichage de l&rsquo;ensemble des mots cl&eacute;s associ&eacute;s &agrave chaque groupe
			(type liste d&eacute;roulante ou case &agrave cocher).<br />
			Le nombre de colonnes pour l&rsquo;affichage des groupes de mots-cl&eacute;s est configurable.',

	//T
	'text_select_groupes_mots_cles' => 'S&eacute;lectionnez un ou plusieurs groupes de mots-cl&eacute;s et choisir le type d&rsquo;affichage voulu sur le site :',
	'text_select_nombre_colonnes' => 'Veuillez s&eacute;lectionner le nombre de colonnes apparaissant dans le mode d&rsquo;affichage case &agrave cocher :',
	'titre_config' => 'Configuration des groupes de mots-cl&eacute;s',
	'titre_config_public' => 'Configuration de l&rsquo;affichage public',

	'titre_page_admin' => 'Configuration du plugin Recherche multicrit&egrave;res 2.0',
);
